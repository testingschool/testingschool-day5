package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ContactFormPage {

    WebDriver driver;

    @FindBy(id = "id_contact")
    private WebElement subjectHeadingDropdown;

    @FindBy(id = "email")
    private WebElement emailInput;

    @FindBy(id = "message")
    private WebElement messageInput;

    @FindBy(id = "submitMessage")
    private WebElement submitButton;

    public ContactFormPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void selectSubjectHeading(String value) {
        Select subjectHeadingSelect = new Select(subjectHeadingDropdown);
        subjectHeadingSelect.selectByVisibleText(value);
    }

    public void inputEmail(String value) {
        emailInput.sendKeys(value);
    }

    public void inputMessage(String value) {
        messageInput.sendKeys(value);
    }

    public void clickSubmit() {
        submitButton.click();
    }

}
