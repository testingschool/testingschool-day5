package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageobjects.ContactFormPage;
import pageobjects.HomePage;

public class MyStepdefs {

    //TODO temporary here
    private WebDriver driver;
    private Object currentPage;

    @Given("^I navigate to \"([^\"]*)\"$")
    public void iNavigateTo(String url) {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver-75.exe");
        driver = new ChromeDriver();
        driver.get(url);
        currentPage = new HomePage(driver);
    }

    @When("^I click on contact link$")
    public void iClickOnContactLink() {
        currentPage = ((HomePage) currentPage).clickContactLink();
    }

    @And("^I complete contact details$")
    public void iCompleteContactDetails() {
        ContactFormPage contactFormPage = (ContactFormPage) currentPage;
        contactFormPage.selectSubjectHeading("Customer service");
        contactFormPage.inputEmail("test@test.com");
        contactFormPage.inputMessage(" My Message");
    }

    @And("^I submit contact form$")
    public void iSubmitContactForm() {
        ((ContactFormPage) currentPage).clickSubmit();
    }

    @Then("^I get a contact form submitted$")
    public void iGetAContactFormSubmitted() {
        driver.quit();
    }
}
